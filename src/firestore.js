import firebase from "@firebase/app"
import "@firebase/firestore"

  // Initialize Firebase
const config = {
    apiKey: "AIzaSyDta0Kma0emnie6Xpjl7wmoCzSNgOIqhsc",
    authDomain: "quick-todo-e124c.firebaseapp.com",
    databaseURL: "https://quick-todo-e124c.firebaseio.com",
    projectId: "quick-todo-e124c",
    storageBucket: "quick-todo-e124c.appspot.com",
    messagingSenderId: "871756815144"
  };

const app = firebase.initializeApp(config);
const firestore = firebase.firestore(app);

export default firestore;